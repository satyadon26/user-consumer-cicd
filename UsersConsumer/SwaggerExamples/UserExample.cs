﻿using Swashbuckle.AspNetCore.Examples;
using UsersConsumer.Models;

namespace UsersConsumer.SwaggerExamples
{
    public class UserExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new User
            {
                UserId = null,
                UserName = "TestUserName",
                DateOfBirth = "01/01/2000"
            };
        }
    }
}
