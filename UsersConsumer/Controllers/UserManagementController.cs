﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Examples;
using UsersConsumer.Services;
using UsersConsumer.SwaggerExamples;
using User = UsersConsumer.Models.User;

namespace UsersConsumer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserManagementController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserManagementController(IUserService userService)
        {
            _userService = userService;
        }

        // GET api/values
        [HttpGet]
        [ProducesResponseType(typeof(IList<User>), 200)]
        [SwaggerOperation(OperationId = "UserManagement_GetAll")]
        [SwaggerResponse(StatusCodes.Status200OK, null, typeof(IList<User>))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<User>>> GetAll()
        {
            var users = await _userService.GetUsers();
            return users.ToList();
        }

        // GET api/values
        [HttpGet("{username}")]
        [ProducesResponseType(typeof(IList<User>), 200)]
        [SwaggerOperation(OperationId = "UserManagement_GetUser")]
        [SwaggerResponse(StatusCodes.Status200OK, null, typeof(User))]
        [SwaggerResponse(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User>> Get(string username)
        {
            var user = await _userService.GetUser(username);
            if (user == null)
                return NotFound();
            return user;
        }

        // POST api/values
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [SwaggerOperation(OperationId = "UserManagement_Add")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status400BadRequest)]
        [SwaggerRequestExample(typeof(User), typeof(UserExample))]
        public async Task<ActionResult> Post([FromBody] User user)
        {
            await _userService.AddUser(user);
            return Ok();
        }

        // PUT api/values/5
        [HttpPut("{username}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [SwaggerOperation(OperationId = "UserManagement_Update")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status400BadRequest)]
        [SwaggerRequestExample(typeof(User), typeof(UserExample))]
        public async Task<ActionResult> Put(string username, [FromBody] User user)
        {
            await _userService.UpdateUser(username, user);
            return Ok();
        }

        // DELETE api/values/5
        [HttpDelete("{username}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [SwaggerOperation(OperationId = "UserManagement_Delete")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status400BadRequest)]
        [SwaggerRequestExample(typeof(User), typeof(UserExample))]
        public async Task<ActionResult> Delete(string username)
        {
            await _userService.DeleteUser(username);
            return Ok();
        }
    }
}
