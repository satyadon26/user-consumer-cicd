﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Users.AutoRest.Client.Proxies;
using User = UsersConsumer.Models.User;

namespace UsersConsumer.Services
{
    public class UserService : IUserService
    {
        private readonly IUsersApi _usersApi;

        public UserService(IUsersApi usersApi)
        {
            _usersApi = usersApi;
        }
        public async Task<IList<Models.User>> GetUsers()
        {
            var users = await _usersApi.Users.GetAsync();
            return MapUsers(users);
        }

        public async Task AddUser(Models.User user)
        {
            var userApiResponse = await _usersApi.User.PostWithHttpMessagesAsync(new Users.AutoRest.Client.Proxies.Models.User(null, user.UserName, user.DateOfBirth));
            if (!userApiResponse.Response.IsSuccessStatusCode)
            {
                throw new Exception("User Api returned error while creating a new user");
            }
        }

        public async Task<Models.User> GetUser(string username)
        {
            var userApiResponse = await _usersApi.User.GetWithHttpMessagesAsync(username);
            if (!userApiResponse.Response.IsSuccessStatusCode)
            {
                throw new Exception("User Api returned error while getting the user");
            }

            return JsonConvert.DeserializeObject<User>(await userApiResponse.Response.Content.ReadAsStringAsync());
        }

        public async Task UpdateUser(string username, Models.User user)
        {
            var userApiResponse = await _usersApi.User.PutWithHttpMessagesAsync(username, new Users.AutoRest.Client.Proxies.Models.User(null, user.UserName, user.DateOfBirth));
            if (!userApiResponse.Response.IsSuccessStatusCode)
            {
                throw new Exception("User Api returned error while updating the user");
            }
        }

        public async Task DeleteUser(string username)
        {
            var userApiResponse = await _usersApi.User.DeleteWithHttpMessagesAsync(username);
            if (!userApiResponse.Response.IsSuccessStatusCode)
            {
                throw new Exception("User Api returned error while deleting the user");
            }
        }
        private IList<Models.User> MapUsers(IList<Users.AutoRest.Client.Proxies.Models.User> users)
        {
            return users.Select(u => new Models.User { UserId = u.UserId, UserName = u.UserName, DateOfBirth = u.DateOfBirth }).ToList();
        }

    }
}
