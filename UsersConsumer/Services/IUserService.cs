﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UsersConsumer.Models;

namespace UsersConsumer.Services
{
    public interface IUserService
    {
        Task AddUser(User user);
        Task<IList<User>> GetUsers();

        Task<User> GetUser(string id);

        Task UpdateUser(string id, User user);

        Task DeleteUser(string id);
    }
}