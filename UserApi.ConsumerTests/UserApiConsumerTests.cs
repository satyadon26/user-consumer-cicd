﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PactNet.Matchers;
using PactNet.Mocks.MockHttpService;
using PactNet.Mocks.MockHttpService.Models;
using Users.AutoRest.Client.Proxies;
using Users.AutoRest.Client.Proxies.Models;
using Xunit;

namespace UserApi.ConsumerTests
{
    public class UserApiConsumerTests : IClassFixture<ConsumerUserApiPact>
    {
        private IMockProviderService _mockProviderService;
        private string _mockProviderServiceBaseUri;
        private UsersApi _usersApi;
        public UserApiConsumerTests(ConsumerUserApiPact data)
        {
            _mockProviderService = data.MockProviderService;
            _mockProviderService.ClearInteractions(); //NOTE: Clears any previously registered interactions before the test is run
            _mockProviderServiceBaseUri = data.MockProviderServiceBaseUri;
            _usersApi = new UsersApi(new Uri(_mockProviderServiceBaseUri));
            _usersApi.HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        [Fact]
        public async Task PostUser_WhenAValidNewUserIsPosted_ReturnsSuccessResponse()
        {
            //Arrange
            _mockProviderService
                .Given("There is no user with username 'testuser'")
                .UponReceiving("A POST request to create new user")
                .With(new ProviderServiceRequest
                {
                    Method = HttpVerb.Post,
                    Path = "/api/users",
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json-patch+json; charset=utf-8" }
                    },
                    Body = new { userName = "testuser", dateOfBirth = "01-Jan-1987" }
                })
                .WillRespondWith(new ProviderServiceResponse
                {
                    Status = 200
                }); //NOTE: WillRespondWith call must come last as it will register the interaction

            //Act
            _usersApi.HttpClient.DefaultRequestHeaders.Clear();
            var headers = new Dictionary<string, List<string>>();
            headers.Add("Accept", new List<string>() { "application/json" });
            var userResponse = await _usersApi.User.PostWithHttpMessagesAsync(new User(userName: "testuser", dateOfBirth: "01-Jan-1987"));

            //Assert
            Assert.True(userResponse.Response.IsSuccessStatusCode);

            _mockProviderService.VerifyInteractions(); //NOTE: Verifies that interactions registered on the mock provider are called at least once
        }

        [Fact]
        public async Task GetUsers_WhenOneUserExists_ReturnsTheSameUser()
        {
            //Arrange
            _mockProviderService
                .Given("There are valid users")
                .UponReceiving("A GET request to retrieve the users")
                .With(new ProviderServiceRequest
                {
                    Method = HttpVerb.Get,
                    Path = "/api/users",
                    Headers = new Dictionary<string, object>
                    {
                        { "Accept", "application/json" }
                    }
                })
                .WillRespondWith(new ProviderServiceResponse
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json; charset=utf-8" }
                    },
                    Body = Match.MinType(new { userId = 1, userName = "amar", dateOfBirth = "01-Jan-1987" }, 1)
                }); //NOTE: WillRespondWith call must come last as it will register the interaction



            //Act
            _usersApi.HttpClient.DefaultRequestHeaders.Clear();
            var headers = new Dictionary<string, List<string>>();
            headers.Add("Accept", new List<string>(){"application/json"});
            var users = await _usersApi.Users.GetWithHttpMessagesAsync(headers);

            //Assert
            Assert.Equal("amar", users.Body.First().UserName);

            _mockProviderService.VerifyInteractions(); //NOTE: Verifies that interactions registered on the mock provider are called at least once        
        }

        [Fact]
        public async Task GetUser_WhenAValidUserExists_ReturnsTheUser()
        {
            //Arrange
            _mockProviderService
                .Given("There is only one user with username 'satya'")
                .UponReceiving("A GET request to retrieve the users")
                .With(new ProviderServiceRequest
                {
                    Method = HttpVerb.Get,
                    Path = "/api/users/satya",
                    Headers = new Dictionary<string, object>
                    {
                        { "Accept", "application/json" }
                    }
                })
                .WillRespondWith(new ProviderServiceResponse
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json; charset=utf-8" }
                    },
                    //Body = Match.Type(new { userId = 1, userName = "satya", dateOfBirth = "01-Jan-1987" })
                    Body = Match.Type(new { userId = 1, userName = "satya", dateOfBirth = "01-Jan-1987", gender = "Male" })
                }); //NOTE: WillRespondWith call must come last as it will register the interaction



            //Act
            _usersApi.HttpClient.DefaultRequestHeaders.Clear();
            var headers = new Dictionary<string, List<string>>();
            headers.Add("Accept", new List<string>() { "application/json" });
            var userResponse = await _usersApi.User.GetWithHttpMessagesAsync("satya", headers);
            var user = (User)userResponse.Body;
            //Assert
            Assert.Equal("satya", user.UserName);

            _mockProviderService.VerifyInteractions(); //NOTE: Verifies that interactions registered on the mock provider are called at least once        
        }

        //[Fact]
        //public async Task UpdateUser_WhenUpdatingAValidUser_ReturnsSuccessResponse()
        //{
        //    //Arrange
        //    _mockProviderService
        //        .Given("There is an existing user with username 'Satya1'")
        //        .UponReceiving("A PUT request to update user")
        //        .With(new ProviderServiceRequest
        //        {
        //            Method = HttpVerb.Put,
        //            Path = "/api/users/Satya1",
        //            Headers = new Dictionary<string, object>
        //            {
        //                { "Content-Type", "application/json-patch+json; charset=utf-8" }
        //            },
        //            Body = new { userName = "Satya1", dateOfBirth = "07-Aug-1986" }
        //        })
        //        .WillRespondWith(new ProviderServiceResponse
        //        {
        //            Status = 200
        //        }); //NOTE: WillRespondWith call must come last as it will register the interaction

        //    //Act
        //    _usersApi.HttpClient.DefaultRequestHeaders.Clear();
        //    var userResponse = await _usersApi.User.PutWithHttpMessagesAsync("Satya1", new User(userName: "Satya1", dateOfBirth: "07-Aug-1986"));

        //    //Assert
        //    Assert.True(userResponse.Response.IsSuccessStatusCode);

        //    _mockProviderService.VerifyInteractions(); //NOTE: Verifies that interactions registered on the mock provider are called at least once
        //}

        //[Fact]
        //public async Task DeleteUser_WhenUpdatingAValidUser_ReturnsSuccessResponse()
        //{
        //    //Arrange
        //    _mockProviderService
        //        .Given("There is an existing user with username 'Satya1'")
        //        .UponReceiving("A Delete request to delete user")
        //        .With(new ProviderServiceRequest
        //        {
        //            Method = HttpVerb.Delete,
        //            Path = "/api/users/Satya1"
        //        })
        //        .WillRespondWith(new ProviderServiceResponse
        //        {
        //            Status = 200
        //        }); //NOTE: WillRespondWith call must come last as it will register the interaction

        //    //Act
        //    _usersApi.HttpClient.DefaultRequestHeaders.Clear();
        //    var userResponse = await _usersApi.User.DeleteWithHttpMessagesAsync("Satya1");

        //    //Assert
        //    Assert.True(userResponse.Response.IsSuccessStatusCode);

        //    _mockProviderService.VerifyInteractions(); //NOTE: Verifies that interactions registered on the mock provider are called at least once
        //}
    }
}
